//
//  LandingPageVC.swift
//  The Jakarta Post
//
//  Created by Wildan Garviandi on 5/27/17.
//  Copyright © 2017 Kellinreaver. All rights reserved.
//

import UIKit

class LandingPageVC: UIViewController {

    @IBOutlet var buttonGetStarted: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        buttonGetStarted.layer.cornerRadius = 12
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func getStartButtonClick(_ sender: Any) {
        let getStartVC = GetStartedVC(nibName: "GetStartedVC", bundle: nil)
        self.navigationController?.pushViewController(getStartVC, animated: true)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
