//
//  GalleryDetail.swift
//  The Jakarta Post
//
//  Created by Wildan Garviandi on 7/25/17.
//  Copyright © 2017 Kellinreaver. All rights reserved.
//

import UIKit

class GalleryDetail: NSObject {
    
    var title: String = ""
    var path_origin: String = ""
    var source: String = ""
    var photographer: String = ""
    
    init(galleryDict:NSDictionary) {
        
        if let title = galleryDict.value(forKey: "title") {
            self.title = title as! String
        }
        
        if let path_origin = galleryDict.value(forKey:"path_origin") {
            self.path_origin = path_origin as! String
        }
        
        if let source = galleryDict.value(forKey: "source") {
            self.source = source as! String
        }
        
        if let photographer = galleryDict.value(forKey: "photographer") {
            self.photographer = photographer as! String
        }
        
    }
}
