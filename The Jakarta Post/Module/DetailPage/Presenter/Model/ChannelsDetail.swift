//
//  ChannelsDetail.swift
//  The Jakarta Post
//
//  Created by Wildan Garviandi on 8/9/17.
//  Copyright © 2017 Kellinreaver. All rights reserved.
//

import UIKit

class ChannelsDetail: NSObject {
    var id:Int = 0
    var name:String = ""
    var parent:String = ""
    
    init(channelsDict:NSDictionary) {
        
        if let id = channelsDict.value(forKey:"id") {
            self.id = id as! Int
        }
        
        if let name = channelsDict.value(forKey: "name") {
            self.name = name as! String
        }
        
        if let parent = channelsDict.value(forKey: "parent") {
            self.parent = parent as! String
        }
        
    }
}
