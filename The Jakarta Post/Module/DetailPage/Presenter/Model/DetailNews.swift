//
//  DetailNews.swift
//  The Jakarta Post
//
//  Created by Wildan Garviandi on 7/16/17.
//  Copyright © 2017 Kellinreaver. All rights reserved.
//

import UIKit

class DetailNews: NSObject {
    
    var published_date: String = ""
    var location: String = ""
    var title: String = ""
    var summary: String = ""
    var content: String = ""
    var gallery:GalleryDetail? = nil
    var channels:ChannelsDetail? = nil
    
    init(newsDict:NSDictionary) {
        if let published_date = newsDict.value(forKey: "published_date") {
            self.published_date = published_date as! String
        }
        
        if let location = newsDict.value(forKey:"location") {
            self.location = location as! String
        }
        
        if let title = newsDict.value(forKey: "title") {
            self.title = title as! String
        }
        
        if let summary = newsDict.value(forKey: "summary") {
            self.summary = summary as! String
        }
        
        if let content = newsDict.value(forKey: "content") {
            self.content = content as! String
        }
        
        if let gallery = newsDict.value(forKey: "gallery") {
            
            let galleryArr = gallery as! NSArray
            let object = GalleryDetail(galleryDict: galleryArr[0] as! NSDictionary)
            self.gallery = object
        }
        
        if let channels = newsDict.value(forKey: "channels") {
            
            let channelsDict = channels as! NSDictionary
            let object = ChannelsDetail(channelsDict: channelsDict)
            self.channels = object
        }
    }

}
