//
//  DetailVC.swift
//  The Jakarta Post
//
//  Created by Wildan Garviandi on 5/30/17.
//  Copyright © 2017 Kellinreaver. All rights reserved.
//

import UIKit

class DetailVC: MasterMVPVC {

    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet var contentView: UIView!
    
    @IBOutlet var labelHeader: UILabel!
    
    @IBOutlet var viewImageHeader: UIView!
    @IBOutlet var imageHeader: UIImageView!
    @IBOutlet var imageHeaderDesc: UILabel!
    
    @IBOutlet var labelContentMain: UILabel!
    
    @IBOutlet var labelSection: UILabel!
    var model:DetailNews? = nil
    var section:String = ""
    
    @IBOutlet var tableViewRelated: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        labelSection.text = section
        
        composeLayout()
    }
    
    func stringFromHTML( string: String?) -> String {
        do {
            let str = try NSAttributedString(data:string!.data(using: String.Encoding.utf8, allowLossyConversion: true
                )!, options:[NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute: NSNumber(value: String.Encoding.utf8.rawValue)], documentAttributes: nil)
            return str.string
        } catch {
            print("html error\n",error)
        }
        
        return ""
    }
    
    func calculateContentHeight(_ width:CGFloat, contentText:String) -> CGFloat{
        let maxLabelSize: CGSize = CGSize(width: width, height: CGFloat(9999))
        let contentNSString = contentText as NSString
        let expectedLabelSize = contentNSString.boundingRect(with: maxLabelSize, options: NSStringDrawingOptions.usesLineFragmentOrigin, attributes: [NSFontAttributeName: UIFont.systemFont(ofSize: 16.0)], context: nil)
        print("\(expectedLabelSize)")
        return expectedLabelSize.size.height
    }
    
    func composeLayout() {
        let widthScreen = self.view.frame.size.width
        let contentText = stringFromHTML(string: model?.content)
        
        //count title
        labelHeader.text = model?.title
        labelHeader.frame.origin.x = 10
        labelHeader.frame.size.width = labelHeader.frame.size.width - 10
        
        //set image header
        let url = URL(string: (model?.gallery?.path_origin)!)
        imageHeader.kf.setImage(with: url)
        imageHeaderDesc.text = model?.gallery?.title
        viewImageHeader.frame.origin.y = labelHeader.bounds.origin.y + (labelHeader.bounds.size.height + 10)
        viewImageHeader.frame.origin.x = 10
        viewImageHeader.frame.size.width = viewImageHeader.frame.size.width - 20
        
        //count content size
        labelContentMain.text = contentText
        labelContentMain.frame.origin.y = labelHeader.frame.size.height + 20
        
        labelContentMain.frame.size = CGSize(width: widthScreen, height: calculateContentHeight(widthScreen, contentText: contentText) + labelHeader.frame.size.height)
        labelContentMain.frame.origin.x = 10
        labelContentMain.frame.size.width = labelContentMain.frame.size.width - 20
        
        contentView.frame.size.height = (labelContentMain.frame.size.height - labelHeader.frame.size.height) + labelHeader.frame.size.height + viewImageHeader.frame.size.height
        scrollView.contentSize = contentView.frame.size
        
        //add content
        contentView.addSubview(labelHeader)
        contentView.addSubview(viewImageHeader)
        contentView.addSubview(labelContentMain)
    }
    
    @IBAction func btnBackTap(_ sender: Any) {
        _ = navigationController?.popViewController(animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
