//
//  MenuTableViewCell.swift
//  The Jakarta Post
//
//  Created by Wildan Garviandi on 7/5/17.
//  Copyright © 2017 Kellinreaver. All rights reserved.
//

import UIKit

class MenuTableViewCell: UITableViewCell {

    @IBOutlet var labelTitle: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
