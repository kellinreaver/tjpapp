//
//  SettingCell.swift
//  The Jakarta Post
//
//  Created by Wildan Garviandi on 8/7/17.
//  Copyright © 2017 Kellinreaver. All rights reserved.
//

import UIKit

class SettingCell: UITableViewCell {

    @IBOutlet var labelSetting: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
