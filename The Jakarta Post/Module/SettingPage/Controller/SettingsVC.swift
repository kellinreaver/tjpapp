//
//  SettingsVC.swift
//  The Jakarta Post
//
//  Created by Wildan Garviandi on 6/1/17.
//  Copyright © 2017 Kellinreaver. All rights reserved.
//

import UIKit

class SettingsVC: MasterMVPVC, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet var settingTableView: UITableView!
    
    var arrAccount = [""]
    var arrDisplay = [""]
    var arrSupport = [""]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        arrAccount = ["Log In Or Create Account", "Subscribe", "Newsletter"]
        arrDisplay =  ["Display Notification", "Font Size", "Section Preferences"]
        arrSupport = ["About The Jakarta Post", "Terms & Condition", "Privacy Policy", "Review on the Apple Store",
                          "Share The Jakarta Post App", "Version 1.0"]
        
        settingTableView.register(UINib(nibName: "SettingCell", bundle: nil), forCellReuseIdentifier: "SettingCell")
        
        settingTableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
        case 0:
            return "ACCOUNT"
        case 1:
            return "DISPLAY"
        case 2:
            return "SUPPORT"
        default:
            return ""
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return arrAccount.count
        case 1:
            return arrDisplay.count
        case 2:
            return arrSupport.count
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SettingCell", for: indexPath) as! SettingCell
        
        switch indexPath.section {
        case 0:
            cell.labelSetting.text = arrAccount[indexPath.row]
            break
        case 1:
            cell.labelSetting.text = arrDisplay[indexPath.row]
            break
        case 2:
            cell.labelSetting.text = arrSupport[indexPath.row]
            break
        default:
            break
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
