//
//  FrontPageVC.swift
//  The Jakarta Post
//
//  Created by Wildan Garviandi on 5/30/17.
//  Copyright © 2017 Kellinreaver. All rights reserved.
//

import UIKit

class FrontPageVC: MasterMVPVC, ISScrollViewPageDelegate {

    
    @IBOutlet var scrollViewPage: ISScrollViewPage!
    
    @IBOutlet var labelSettings: UILabel!
    @IBOutlet var buttonDone: UIButton!
    @IBOutlet var labelJKP: UILabel!
    @IBOutlet var sideButton: UIButton!
    @IBOutlet var settingsButton: UIButton!
    
    var arrTopics = [TopicsModel]()
    
    private var isSideMenuShown:Bool = false
    private var isSettingsShown:Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let controllers:NSMutableArray = []
        
        for model in arrTopics {
            let listNewsController = ListNewsVC(nibName: "ListNewsVC", bundle: nil)
            listNewsController.section = model.topics
            listNewsController.sectionTitle = model.topicsName
            self.addChildViewController(listNewsController)
            controllers.add(listNewsController)
        }

        self.scrollViewPage.setPaging(true)
        self.scrollViewPage.scrollViewPageDelegate = self
        self.scrollViewPage.scrollViewPageType = ISScrollViewPageType.horizontally
        self.scrollViewPage.setControllers(controllers as AnyObject as! [UIViewController])
        
        // Do any additional setup after loading the view.
    }
    
    func scrollViewPageDidScroll(_ scrollView: UIScrollView) {
        
    }
    
    func scrollViewPageDidChanged(_ scrollViewPage: ISScrollViewPage, index: Int) {
        labelJKP.text = arrTopics[index].topicsName
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func sideMenuClick(_ sender: Any) {
        let sideMenuVC = SideMenuVC(nibName: "SideMenuVC", bundle: nil)
        if (!isSideMenuShown) {
            isSideMenuShown = true
            sideMenuVC.view.frame.origin = CGPoint(x: 0, y: 64)
            sideMenuVC.view.frame.size = CGSize(width: self.view.frame.size.width, height: self.view.frame.size.height)
            sideMenuVC.view.tag = 20
            sideMenuVC.topics = arrTopics
            self.view.addSubview(sideMenuVC.view)
        } else {
            if let viewWithTag = self.view.viewWithTag(20) {
                viewWithTag.removeFromSuperview()
                isSideMenuShown = false
            }
        }
    }

    @IBAction func doneButtonSettingClick(_ sender: Any) {
        if let viewTag = self.view.viewWithTag(21) {
            viewTag.removeFromSuperview()
            isSettingsShown = false
            labelSettings.isHidden = true
            buttonDone.isHidden = true
            isShowSettings(false)
        }
    }
    
    @IBAction func settingClick(_ sender: Any) {
        let settingsVC = SettingsVC(nibName: "SettingsVC", bundle: nil)
        if (!isSettingsShown) {
            isSettingsShown = true
            settingsVC.view.frame = CGRect(x: 0, y: 64, width: self.view.frame.size.width, height: self.view.frame.size.height - 64)
            settingsVC.view.tag = 21
            labelSettings.isHidden = false
            buttonDone.isHidden = false
            isShowSettings(true)
            self.view.addSubview(settingsVC.view)
        }
    }
    
    private func isShowSettings(_ settingShown:Bool) {
        labelJKP.isHidden = settingShown
        sideButton.isHidden = settingShown
        settingsButton.isHidden = settingShown
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
