//
//  ListNewsVC.swift
//  The Jakarta Post
//
//  Created by Wildan Garviandi on 7/5/17.
//  Copyright © 2017 Kellinreaver. All rights reserved.
//

import UIKit

class ListNewsVC: MasterMVPVC, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    @IBOutlet var collectionView: UICollectionView!
    
    var arrRaw:NSMutableArray = []
    var section:String = ""
    var sectionTitle:String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        registerCell()

        NSLog("List section \(section)")
        networkManager.getListNews(section, limit: 40, offset: 0) { (resultArr) in
            for object in resultArr {
                let model = DetailNews(newsDict: object as! NSDictionary)
                self.arrRaw.add(model)
            }

            self.collectionView.reloadData()
        }
        // Do any additional setup after loading the view.
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrRaw.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let screenWidth = self.view.frame.size.width
        
        if (section == "photos" || section == "videos") {
            return CGSize(width: screenWidth, height: 230);
        } else {
            switch indexPath.row {
            case 0:
                return CGSize(width: screenWidth, height: 213);
            case 1:
                return CGSize(width: screenWidth, height: 213);
            case 2:
                return CGSize(width: screenWidth, height: 100);
            case 8:
                return CGSize(width: screenWidth, height: 230);
            default:
                return CGSize(width: screenWidth, height: 100);
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let model = arrRaw[indexPath.row] as! DetailNews
        
        if (section == "photos" || section == "videos") {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MediaCollectionViewCell",
                                                          for: indexPath) as! MediaCollectionViewCell
            let url = URL(string: (model.gallery?.path_origin)!)
            cell.imageView.kf.setImage(with: url)
            cell.labelTitle.text = model.title
            cell.labelDesc.text = model.summary
            return cell
        } else {
            switch indexPath.row {
            case 0:
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell1CollectionViewCell",
                                                              for: indexPath) as! Cell1CollectionViewCell
                let date = convertStrinToDate(model.published_date)
                let url = URL(string: (model.gallery?.path_origin)!)
                cell.imageView.kf.setImage(with: url)
                cell.labelTitle.text = model.title
                cell.labelDesc.text = " • \(dayDifference(from: (date.timeIntervalSinceNow)))"
                cell.labelLocation.text = "\(model.location)"
                return cell
            case 1:
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell2CollectionViewCell",
                                                              for: indexPath) as! Cell2CollectionViewCell
                let date = convertStrinToDate(model.published_date)
                let url = URL(string: (model.gallery?.path_origin)!)
                cell.imageViewKiri.kf.setImage(with: url)
                cell.labelTitleKiri.text = "\(model.channels!.name) • \(dayDifference(from: (date.timeIntervalSince(date))))"
                cell.labelDescKiri.text = model.title
                
                cell.imageViewKanan.kf.setImage(with: url)
                cell.labelTitleKanan.text = "\(model.channels!.name) • \(dayDifference(from: (date.timeIntervalSince(date))))"
                cell.labelDescKanan.text = model.title
                return cell
            case 2:
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell3CollectionViewCell",
                                                              for: indexPath) as! Cell3CollectionViewCell
                let date = convertStrinToDate(model.published_date)
                let url = URL(string: (model.gallery?.path_origin)!)
                cell.imageView.kf.setImage(with: url)
                cell.labelTopics.text = "\(model.channels!.name)"
                cell.labelTitle.text = " • \(dayDifference(from: (date.timeIntervalSince(date))))"
                cell.labelDesc.text = model.title
                return cell
            default:
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell3CollectionViewCell",
                                                              for: indexPath) as! Cell3CollectionViewCell
                let date = convertStrinToDate(model.published_date)
                let url = URL(string: (model.gallery?.path_origin)!)
                cell.imageView.kf.setImage(with: url)
                cell.labelTitle.text = "\(model.channels!.name) • \(dayDifference(from: (date.timeIntervalSince(date))))"
                cell.labelDesc.text = model.title
                return cell
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let model = arrRaw[indexPath.row] as! DetailNews
        let detailVC = DetailVC(nibName: "DetailVC", bundle: nil)
        detailVC.model = model
        detailVC.section = sectionTitle
        self.navigationController?.pushViewController(detailVC, animated: true)
    }
    
    private func convertStrinToDate(_ date:String) -> Date {
        let formatter = DateFormatter()//2017-07-28 19:02:17
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        formatter.timeZone = TimeZone(identifier: "Indonesia/Jakarta")
        let strDate = formatter.date(from: date)
        
        return strDate!
    }
    
    private func registerCell() {
        collectionView.register(UINib(nibName: "Cell1CollectionViewCell", bundle: nil),
                                forCellWithReuseIdentifier: "Cell1CollectionViewCell")
        collectionView.register(UINib(nibName: "Cell2CollectionViewCell", bundle: nil),
                                forCellWithReuseIdentifier: "Cell2CollectionViewCell")
        collectionView.register(UINib(nibName: "Cell3CollectionViewCell", bundle: nil),
                                forCellWithReuseIdentifier: "Cell3CollectionViewCell")
        collectionView.register(UINib(nibName: "MediaCollectionViewCell", bundle: nil),
                                forCellWithReuseIdentifier: "MediaCollectionViewCell")
    }
    
    func dayDifference(from interval : TimeInterval) -> String {
        let calendar = NSCalendar.current
        let date = Date(timeIntervalSince1970: interval)
        if calendar.isDateInYesterday(date) { return "Yesterday" }
        else if calendar.isDateInToday(date) { return "Today" }
        else if calendar.isDateInTomorrow(date) { return "Tomorrow" }
        else {
            let startOfNow = calendar.startOfDay(for: Date())
            let startOfTimeStamp = calendar.startOfDay(for: date)
            let components = calendar.dateComponents([.day], from: startOfNow, to: startOfTimeStamp)
            let day = components.day!
            if day < 1 { return "\(abs(day)) days ago" }
            else { return "In \(day) days" }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
