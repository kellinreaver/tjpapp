//
//  Cell3CollectionViewCell.swift
//  The Jakarta Post
//
//  Created by Wildan Garviandi on 5/30/17.
//  Copyright © 2017 Kellinreaver. All rights reserved.
//

import UIKit

class Cell3CollectionViewCell: UICollectionViewCell {

    @IBOutlet var labelDesc: UILabel!
    @IBOutlet var labelTitle: UILabel!
    @IBOutlet var imageView: UIImageView!
    @IBOutlet var labelTopics: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
