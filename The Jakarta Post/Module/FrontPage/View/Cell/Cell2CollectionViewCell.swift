//
//  Cell2CollectionViewCell.swift
//  The Jakarta Post
//
//  Created by Wildan Garviandi on 5/30/17.
//  Copyright © 2017 Kellinreaver. All rights reserved.
//

import UIKit

class Cell2CollectionViewCell: UICollectionViewCell {

    @IBOutlet var labelDescKiri: UILabel!
    @IBOutlet var labelTitleKiri: UILabel!
    @IBOutlet var imageViewKiri: UIImageView!
    
    @IBOutlet var imageViewKanan: UIImageView!
    @IBOutlet var labelTitleKanan: UILabel!
    @IBOutlet var labelDescKanan: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
