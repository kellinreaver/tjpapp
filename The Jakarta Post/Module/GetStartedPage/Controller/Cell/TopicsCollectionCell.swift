//
//  TopicsCollectionCell.swift
//  The Jakarta Post
//
//  Created by Wildan Garviandi on 5/27/17.
//  Copyright © 2017 Kellinreaver. All rights reserved.
//

import UIKit

class TopicsCollectionCell: UICollectionViewCell {

    @IBOutlet var labelTopics: UILabel!
    @IBOutlet var imgTopics: UIImageView!
    @IBOutlet var ximage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    public func setDataToCell(_ model:TopicsModel) {
        imgTopics.image = UIImage(named: model.imageUrl)
        labelTopics.text = model.topicsName
    }

}
