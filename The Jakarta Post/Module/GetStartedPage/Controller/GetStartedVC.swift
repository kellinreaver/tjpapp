//
//  GetStartedVC.swift
//  The Jakarta Post
//
//  Created by Wildan Garviandi on 5/27/17.
//  Copyright © 2017 Kellinreaver. All rights reserved.
//

import UIKit
import PKHUD

class GetStartedVC: MasterMVPVC, UICollectionViewDataSource, UICollectionViewDelegate {

    @IBOutlet var selectedLabel: UILabel!
    @IBOutlet var collectionTopics: UICollectionView!
    @IBOutlet var nextButton: UIButton!
    
    let arrTopics:NSMutableArray = []
    let arrSelectedTopics:NSMutableArray = []
    var selectedCell = [TopicsModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        nextButton.layer.cornerRadius = 12
        
        // Do any additional setup after loading the view.
        collectionTopics.register(UINib(nibName: "TopicsCollectionCell", bundle: nil), forCellWithReuseIdentifier: "TopicsCollectionCell")
        collectionTopics.allowsMultipleSelection = true
        collectionTopics.allowsSelection = true
        
        
        networkManager.getTopics { (topicsDict) in
            for topics in topicsDict {
                let model = TopicsModel(topicsDict: topics as! NSDictionary)
                self.arrTopics.add(model)
            }
        }
        
        for model in arrTopics {
            selectedCell.append(model as! TopicsModel)
            arrSelectedTopics.add(model)
        }
        
        selectedLabel.text = setLabelSelected(selectedCell.count)
        
        collectionTopics.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let model = arrTopics[indexPath.row] as! TopicsModel
        arrSelectedTopics.remove(model)
        
        let cell = collectionView.cellForItem(at: indexPath) as! TopicsCollectionCell
        cell.imgTopics.alpha = 0.5
        cell.backgroundColor = UIColor.red
        
        if selectedCell.contains(model) {
            selectedCell.remove(at: selectedCell.index(of: model)!)
            cell.contentView.backgroundColor = .red
            cell.ximage.isHidden = false
        }
        
        selectedLabel.text = setLabelSelected(selectedCell.count)
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        let model = arrTopics[indexPath.row] as! TopicsModel
        arrSelectedTopics.add(model)
        selectedCell.append(model)
        
        let cell = collectionView.cellForItem(at: indexPath) as! TopicsCollectionCell
        cell.imgTopics.alpha = 1
        cell.backgroundColor = UIColor.white
        cell.ximage.isHidden = true
        
        selectedLabel.text = setLabelSelected(selectedCell.count)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrTopics.count
    }
    
    func setLabelSelected(_ number:Int) -> String {
        return "(\(number)) SELECTED"
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TopicsCollectionCell", for: indexPath) as! TopicsCollectionCell
        
        let model = arrTopics[indexPath.row] as! TopicsModel
        
        if selectedCell.contains(model) {
            cell.contentView.backgroundColor = .red
            cell.ximage.isHidden = true
        } else {
            cell.contentView.backgroundColor = .red
            cell.ximage.isHidden = false
        }
        
        cell.setDataToCell(arrTopics[indexPath.row] as! TopicsModel)
        
        return cell
    }

    @IBAction func buttonNextTap(_ sender: Any) {
        if (selectedCell.count > 3) {
            let frontPageVC = FrontPageVC(nibName: "FrontPageVC", bundle: nil)
            frontPageVC.arrTopics = selectedCell
            self.navigationController?.pushViewController(frontPageVC, animated: true)
        } else {
            HUD.show(.label("Please select topics more than 3"))
            HUD.hide(afterDelay: 1.5)
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
