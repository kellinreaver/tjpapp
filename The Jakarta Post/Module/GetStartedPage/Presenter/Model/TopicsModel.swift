//
//  TopicsModel.swift
//  The Jakarta Post
//
//  Created by Wildan Garviandi on 5/28/17.
//  Copyright © 2017 Kellinreaver. All rights reserved.
//

import UIKit

class TopicsModel: NSObject {
    var topicsId: String = ""
    var imageUrl: String = ""
    var topicsName: String = ""
    var topics: String = ""
    
    init(topicsDict:NSDictionary) {
        if let topicsId = topicsDict.value(forKey: "topicsId") {
            self.topicsId = topicsId as! String
        }
        
        if let imageUrl = topicsDict.value(forKey:"imageUrl") {
            self.imageUrl = imageUrl as! String
        }
        
        if let topicsName = topicsDict.value(forKey: "topicsName") {
            self.topicsName = topicsName as! String
        }
        
        if let topics = topicsDict.value(forKey: "topics") {
            self.topics = topics as! String
        }
    }
}
