//
//  GetStartedView.swift
//  The Jakarta Post
//
//  Created by Wildan Garviandi on 5/30/17.
//  Copyright © 2017 Kellinreaver. All rights reserved.
//

import UIKit

protocol Bird {
    var name: String { get }
    var canFly: Bool { get }
}
