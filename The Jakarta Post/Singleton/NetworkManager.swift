//
//  NetworkManager.swift
//  The Jakarta Post
//
//  Created by Wildan Garviandi on 5/28/17.
//  Copyright © 2017 Kellinreaver. All rights reserved.
//

import UIKit
import AlamofireDomain

class NetworkManager: NSObject {
    
    public var isMockingEnable = true

    //MARK: Shared Instance
    
    private override init() { }
    
    public static func Instance() -> NetworkManager {
        return instance
    }
    
    public static var TheInstance : NetworkManager {
        get { return instance }
    }
    
    static let instance : NetworkManager = NetworkManager()
    var state = 0
    
    let baseUrl = "http://api.thejakartapost.com/v1/"
    
    /**
     About Us webview uri
     
     - returns: return url about us view
     */
    internal func aboutUsUrl() -> String {
        return ""
    }
    
    internal func authLogin(_ email:String, pass:String, completion: @escaping (_ resultDict: NSDictionary) -> Void) {
        if isMockingEnable {
            
        } else {
            let url = "\(baseUrl)auth/login"
            
            let headers = [
                "Content-Type": "application/x-www-form-urlencoded"
            ]
            
            let parameters = [
                "email":email,
                "password":pass
            ]
            
            AlamofireDomain.request(url, parameters: parameters, encoding: URLEncoding.default, headers: headers)
                .responseJSON { (response) in
                    if let error = response.result.error {
                        completion(["error":error])
                    }
                    
                    if let JSON = response.result.value {
                        completion(JSON as! NSDictionary)
                    }
            }
            
        }
    }
    
    /**
     get topics for getstarted
     
     - parameter modelTopics: model in request
     - parameter completion: return dictionary of result
     */
    internal func getTopics(_ completion: @escaping (_ resultDict: NSArray) -> Void) {
        if isMockingEnable {
            let path = Bundle.main.path(forResource: "jsonTopics", ofType: "json")
            let jsonData: NSData = NSData(contentsOfFile: path!)!
            
            do {
                let jsonDict = try JSONSerialization.jsonObject(with: jsonData as Data,
                                                                          options:JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                
                completion(jsonDict["item"] as! NSArray)
            } catch {
                NSLog("failed parse JSON")
            }
        }
    }
    
    internal func getListNews(_ section:String, limit:Int, offset:Int, completion: @escaping (_ resultArr:NSArray) -> Void) {
        var sectionTitle = ""
        
        switch section {
            case "home":
                sectionTitle = "home"
                break
            case "news":
                sectionTitle = "news"
                break
            case "bussines":
                sectionTitle = "news/business"
                break
            case "sea":
                sectionTitle = "seasia"
                break
            case "life":
                sectionTitle = "life"
                break
            case "travel":
                sectionTitle = "travel"
                break
            case "most_viewed":
                sectionTitle = "most_viewed"
                break
            default:
                sectionTitle = "home"
                break
        }
        
        let url = URL(string: "\(baseUrl)articles/\(sectionTitle)/?limit=\(limit)&skip=\(offset)")!
        NSLog("url \(url)")

        AlamofireDomain.request(url).responseJSON { (response) in
            if let error = response.result.error {
                completion([error])
            }
            
            if let JSON = response.result.value {
                let result = JSON as! NSDictionary
                if (result["response"] as! String == "ok") {
                    completion(result["data"] as! NSArray)
                }
            }
        }
    }
}
